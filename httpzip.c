#include "httpzip.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <mz.h>
#include <mz_zip.h>
#include <mz_strm.h>
#include <mz_zip_rw.h>

#ifdef WIN32
#include <winsock2.h>
#include <WS2tcpip.h>
#define get_error_code() WSAGetLastError()
#else
//This isn't supposed to work on Linux but... Maybe an AppImage might work.
#include <sys/socket.h>
#define get_error_code() errno()
#endif


static const struct hz_config_s* config = NULL;

/// @brief Test if zipfile is readable, optionally with error check.
static int check_zipfile(const char* file, int fast_test)
{
	void* zip_handle = NULL;
	int zip_rv;
	int rv = RV_OK;

	//Open ZIP handle
	mz_zip_create(&zip_handle);
	if (zip_handle == NULL)
	{
		if (config->verbosity >= HZ_VERBOSITY_ERROR)
			printf("[ERROR] Unable to create minizip handle\r\n");
		return RV_NOK;
	}

	//Create zipfile reader
	zip_rv = mz_zip_reader_open_file(zip_handle, file);
	if (zip_rv != MZ_OK)
	{
		if (config->verbosity >= HZ_VERBOSITY_ERROR)
			printf("[ERROR] Unable to open zipfile <%s>\r\n", file);
		return RV_NOK;
	}
	else
	{
		if (config->verbosity >= HZ_VERBOSITY_DEBUG)
			printf("[DEBUG] Zipfile <%s> open\r\n", file);
	}

	//Iterate over files and check them
	if(!fast_test)
	{
		if (config->verbosity >= HZ_VERBOSITY_INFO)
		{
			printf("[INFO] Performing full integrity check (Unimplemented!)\r\n");
		}

		zip_rv = mz_zip_reader_goto_first_entry(zip_handle);
		if (zip_rv != MZ_OK)
		{
			return RV_NOK;
		}

		do
		{
			//Get file info
			mz_zip_file* fileinfo;
			zip_rv = mz_zip_reader_entry_get_info(zip_handle, &fileinfo);
			rv = ((rv == RV_OK) && (zip_rv == MZ_OK)) ? RV_OK : RV_NOK;

			//CRC check (ToDo)
			{

			}

			//Show test info
			if (config->verbosity >= HZ_VERBOSITY_DEBUG)
			{
				printf("[DEBUG] %s %s (%s)\r\n",
					mz_zip_reader_entry_is_dir(zip_handle) ? "F" : "D",
					fileinfo->filename,
					mz_zip_get_compression_method_string(fileinfo->compression_method)
				);
			}
		}
		while (mz_zip_reader_goto_next_entry(zip_handle) == MZ_OK);
	}

	//Cleanup
	mz_zip_reader_close(zip_handle);
	mz_zip_delete(&zip_handle);
	return rv;
}

// -----------------------------------
// --- All of the HTTP server code ---
// -----------------------------------

static const char* default_headers[] =
{
	"X-Dummy: Sample header\r\n",
	"Cache-control: public, max-age=3600\r\n",
	"Content-Security-Policy: default-src 'self'; object-src 'none'; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-eval'; media-src 'self' blob:; base-uri 'self'; form-action 'self'; frame-ancestors 'self'\r\n",
	NULL,
};

enum e_response_type {
	RESPONSE_DEFAULT,
	HTTP_REQUEST_HEADER_TOO_LARGE = 431, //< Request Header Fields Too Large
	HTTP_IM_A_TEAPOT = 418, //< I'm a teapot!
};

struct s_http_name {
	int code;
	const char* name;
};

struct s_http_name http_names[] = {
	{418, "I'm a teapot!"},
	{ 0, "STATUS" } //< Default code name
};

static const char* get_http_name(int code)
{
	struct s_http_name* name = http_names;
	while (name->code) {
		if (name->code == code)
			break;
		name++;
	}
	return name->name;
}

static void send_response(SOCKET client_socket, const char* arg, int response_type)
{
	static char response_buffer[1024];
	//Canned responses (HTTP status codes)
	if (response_type != RESPONSE_DEFAULT)
	{
		//HTTP response
		sprintf(response_buffer, "HTTP/1.0 %d %s\r\n", response_type, get_http_name(response_type));
		send(client_socket, response_buffer, strlen(response_buffer), 0);
		//Default headers
		const char** header = default_headers;
		while (*header)
		{
			size_t header_size = strlen(*header);
			send(client_socket, *header, (int)header_size, 0);
			header++;
		}
		//Content-type header (And end-of-headers)
		{
			const char str[] = "Content-Type: text/html\r\n" "\r\n";
			send(client_socket, str, sizeof(str) - 1, 0);
		}
		//Canned HTML
		{
			sprintf(response_buffer, "<html><body><head><title>HTTP %d</title></head><h1>HTTP %d</h1><h2>%s</h2><p>%s</p></body></html>",
				response_type,
				response_type,
				get_http_name(response_type),
				arg ? arg : ""
			);
			send(client_socket, response_buffer, strlen(response_buffer), 0);
		}
		return;
	}

	//Send data from zipfile
	//ToDo
}

void hz_launch(const struct hz_config_s *conf)
{
	assert(conf);
	config = conf;

	//Check zipfile
	if (check_zipfile(config->zipfile, config->fast_check) == RV_OK)
	{
		//Check OK
		if (config->verbosity >= HZ_VERBOSITY_DEBUG)
			printf("[DEBUG] ZIP selftest OK\r\n");
	}
	else
	{
		//Check failed
		if (config->verbosity >= HZ_VERBOSITY_ERROR)
			printf("[ERROR] ZIP selftest failed\r\n");
		return;
	}

	//Create socket
	SOCKET server_socket;
	if (conf->use_ipv6)
	{
		server_socket = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
	}
	else
	{
		server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}
	if (server_socket == INVALID_SOCKET)
	{
		int err = get_error_code();
		if (config->verbosity >= HZ_VERBOSITY_ERROR)
			printf("[ERROR] Unable to create socket; Reason: %d\r\n", err);
		return;
	}
	//Bind it
	{
		int err;
		
		void* service = NULL;
		int service_size;
		//Defaults (IPv6)
		SOCKADDR_IN6 service_six;
		{
			memset(&service_six, 0, sizeof(SOCKADDR_IN6));
			service_six.sin6_family = AF_INET6;
			service_six.sin6_addr = in6addr_loopback;
			service_six.sin6_port = htons(0);
		}
		//Defaults (IPv4)
		SOCKADDR_IN service_four;
		{
			memset(&service_four, 0, sizeof(SOCKADDR_IN));
			service_four.sin_family = AF_INET;
			service_four.sin_addr.S_un.S_addr = INADDR_LOOPBACK;
			service_four.sin_port = htons(0);
		}
		//Pick default
		service = &service_six;
		service_size = sizeof(SOCKADDR_IN6);

		//Use config attributes (ToDo)
		{
			//--port;
			service_six.sin6_port = htons(conf->port);
			service_four.sin_port = htons(conf->port);
			
			//--ipv4
			if (conf->use_ipv4) {
				service = &service_four;
				service_size = sizeof(SOCKADDR_IN);
			}

			//--ipv6
			if (conf->use_ipv6) {
				service = &service_six;
				service_size = sizeof(SOCKADDR_IN6);
			}

			//--bind
			// ToDo
		}

		//Bind
		SOCKADDR_IN6 bound_service_six;
		memset(&bound_service_six, 0, sizeof(SOCKADDR_IN6));
		SOCKADDR_IN bound_service_four;
		memset(&bound_service_four, 0, sizeof(SOCKADDR_IN));
		int bound_service_size = conf->use_ipv6 ? sizeof(SOCKADDR_IN6) : sizeof(SOCKADDR_IN);

		char address_str[INET6_ADDRSTRLEN + 1] = "";
		{
			err = bind(server_socket, service, service_size);
			if (err)
			{
				if (config->verbosity >= HZ_VERBOSITY_ERROR)
					printf("[ERROR] Unable to bind socket; Reason: %d\r\n", get_error_code());
			}
			else
			{
				//Get the bound socket info
				if (conf->use_ipv6)
				{
					//IPv6
					getsockname(server_socket, (void*)&bound_service_six, &bound_service_size);
					inet_ntop(AF_INET6, &bound_service_six.sin6_addr, address_str, sizeof(address_str));
				}
				else
				{
					//IPv4
					getsockname(server_socket, (void*)&bound_service_four, &bound_service_size);
					inet_ntop(AF_INET, &bound_service_four.sin_addr, address_str, sizeof(address_str));
				}

				if (config->verbosity >= HZ_VERBOSITY_DEBUG)
				{
					printf("[DEBUG] Bound port: %d\r\n", 
						ntohs((conf->use_ipv6) ? bound_service_six.sin6_port : bound_service_four.sin_port)
					);
					printf("[DEBUG] Bound address: %s\r\n", address_str);
				}
			}
		}

		//Launch browser
		{
			char url[INET6_ADDRSTRLEN + 32];
			if (conf->use_ipv6)
			{
				//IPv6
				sprintf(url, "http://[%s]:%d/", address_str, ntohs(bound_service_six.sin6_port));
			}
			else
			{
				//IPv4
				sprintf(url, "http://%s:%d/", address_str, ntohs(bound_service_four.sin_port));
			}
			ShellExecute(NULL, "open", url, NULL, NULL, SW_SHOWNORMAL);
		}

		//Listen
		int rv = listen(server_socket, SOMAXCONN);
		int keep_serving = 1;
		if (rv != 0)
		{
			if (config->verbosity >= HZ_VERBOSITY_ERROR)
				printf("[ERROR] Failed to set socket to LISTEN\r\n");
			return;
		}

		//Await responses and serve forever
		do
		{
			SOCKADDR_IN6 accepted_request;
			memset(&accepted_request, 0, sizeof(SOCKADDR_IN6));
			char client_address_str[INET6_ADDRSTRLEN] = "";

			SOCKET client_socket = accept(server_socket, (void*) & accepted_request, &bound_service_size);
			if (client_socket == INVALID_SOCKET)
			{
				if (config->verbosity >= HZ_VERBOSITY_ERROR)
					printf("[ERROR] Unable to accept connections; Reason: %d\r\n", get_error_code());
				break;
			}
			else
			{
				inet_ntop(AF_INET6, &accepted_request.sin6_addr, client_address_str, INET6_ADDRSTRLEN);
				if (config->verbosity >= HZ_VERBOSITY_DEBUG)
				{
					printf("[DEBUG] Connection from [%s]:%d\r\n", client_address_str, accepted_request.sin6_port);
				}
			}

			//Browser has sent us a request, parse it. (ToDo)
			{
				char rcv_buffer[10240];
				int rcv_size = recv(client_socket, rcv_buffer, sizeof(rcv_buffer) - 1, 0);
				rcv_buffer[rcv_size] = '\0';
				if (config->verbosity >= HZ_VERBOSITY_DEBUG)
				{
					printf("[DEBUG >]\r\n");
					puts(rcv_buffer);
					printf("[DEBUG <]\r\n");
				}
			}

			//Send back a response with file contents. (ToDo)
			{
				send_response(client_socket, "Unimplemented", HTTP_IM_A_TEAPOT);
			}

			//Close connection
			closesocket(client_socket);
		} while (keep_serving);
	}
}
