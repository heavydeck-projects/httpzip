#ifndef __HTTPZIP_H
#define __HTTPZIP_H

#include <stdint.h>

#define RV_OK 0
#define RV_NOK 1
#define RV_BAD_OPTIONS 2

enum hz_verbosity {
	HZ_VERBOSITY_MUTED,
	HZ_VERBOSITY_CRITICAL,
	HZ_VERBOSITY_ERROR,
	HZ_VERBOSITY_WARNING,
	HZ_VERBOSITY_INFO,
	HZ_VERBOSITY_REQUEST,
	HZ_VERBOSITY_DEBUG,
	HZ_VERBOSITY_ALL
};
#ifdef NDEBUG
// --- Release-specific configurations ---

//Release versions need to bother only with Warnings or above
#define HZ_VERBOSITY_DEFAULT HZ_VERBOSITY_WARNING

//Release will read itself by default
#define HZ_DEFAULT_ZIPFILE NULL

//Debug performs thorought test
#define HZ_DEFAULT_CHECK TRUE

#else
// --- Debug-specific configurations

//Debug will show the kitchen sink by default
#define HZ_VERBOSITY_DEFAULT HZ_VERBOSITY_ALL

//Debug will read "sample.zip" instead of itself by default
#define HZ_DEFAULT_ZIPFILE "sample.zip"

//Debug performs thorought test
#define HZ_DEFAULT_CHECK 0
#endif

//Use IPv6 by default or not
#define HZ_DEFAULT_IPV6 1

struct hz_config_s
{
	//General options
	enum hz_verbosity verbosity;
	int fast_check;

	//Server config
	uint_fast16_t port;
	const char* bind_address;
	const char* zipfile;
	int use_ipv4;
	int use_ipv6;

	//Creation options
	const char* in_name;
	const char* out_name;
};

void hz_launch(const struct hz_config_s *conf);

#endif
