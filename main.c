/*
* Main file: This file parses the command line arguments and prepares
* the environment for the program execution.
* 
* (This file must be portable and compile outside Windows)
*/

#include <stdio.h>
#include <assert.h>
#include "httpzip.h"

#define OPTPARSE_IMPLEMENTATION
#define OPTPARSE_API static
#include "optparse.h"

#include <mz.h>
#include <mz_zip.h>
#include <zlib.h>

#ifdef _WIN32
#include <windows.h>
//#include <winsock2.h>
/// @brief Launch runtime checks specific to Windows.
static void win_startup_checks(const struct hz_config_s* conf)
{
	//Check UTF8 codepage
	unsigned int cp = GetACP();
	if(cp != CP_UTF8)
	{
		CPINFOEXA cpinfo;
		memset(&cpinfo, 0, sizeof(CPINFOEXA));
		GetCPInfoExA(cp, 0, &cpinfo);
		if (conf->verbosity >= HZ_VERBOSITY_WARNING)
			printf("[WARNING] Program not runnig with UTF-8 codepage; Current codepage %s (%u)\r\n",
				cpinfo.CodePageName,
				cp
		);
	}

	//Launch Winso
	{
		WSADATA wsaData;
		int err;
		WORD wVersionRequested = MAKEWORD(2, 2);
		err = WSAStartup(wVersionRequested, &wsaData);
		if (err)
		{
			if (conf->verbosity >= HZ_VERBOSITY_ERROR)
				printf("[ERROR] Winsock failed to start; Reason: %d\r\n", WSAGetLastError());
		}
		else
		{
			if (conf->verbosity >= HZ_VERBOSITY_DEBUG)
				printf("[DEBUG] Winsock OK\r\n");
		}
	}
}
#endif

/// @brief Launch runtime checks.
static int startup_checks(const struct hz_config_s* conf)
{
#ifdef _WIN32
	//Windows specific checks
	win_startup_checks(conf);
#endif
	int rv = RV_OK;

	//Check if we ourselves are readable
	{
		FILE* us = fopen(conf->zipfile, "rb");
		if(us == NULL)
		{
			if(conf->verbosity >= HZ_VERBOSITY_ERROR)
				printf("[ERROR] executable/zipfile <%s> is not readable\r\n", conf->zipfile);
			rv = RV_NOK;
		}
		else
		{
			fclose(us);
		}
	}

	//Check Zlib
	{
		if (!zlibVersion())
		{
			if (conf->verbosity >= HZ_VERBOSITY_ERROR)
				printf("[ERROR] zlib doesn't seem to be working\r\n");
			rv = RV_NOK;
		}
	}

	//Check minizip
	{
		void* zip_handle = NULL;
		mz_zip_create(&zip_handle);
		if (!zip_handle)
		{
			if (conf->verbosity >= HZ_VERBOSITY_ERROR)
				printf("[ERROR] minizip-ng doesn't seem to be working\r\n");
			rv = RV_NOK;
		}
		else
		{
			mz_zip_delete(&zip_handle);
		}
	}

	//More extra info, for debugging
	if(conf->verbosity >= HZ_VERBOSITY_DEBUG)
	{
		//Current working directory
		{
			char cwd[MAX_PATH + 1];
			getcwd(cwd, sizeof(cwd));
			printf("[DEBUG] CWD: %s\r\n", cwd);
		}
	}

	return rv;
}

struct opt_description {
	int shortname;
	const char* argument_description;
	const char* description;
};

struct opt_description options_description[] =
{
	{'o',    "FILE", "Use FILE for output instead of the default 'zipfile.exe'."},
	{'z', "ZIPFILE", "Serve ZIPFILE instead of executable embedded ZIP data."},
	{'p',    "PORT", "Listen on PORT; Random vailable port by default."},
	{'b', "ADDRESS", "Bind to ADDRESS; Localhost by default."},
	{'q',      NULL, "Show no output, be quiet."},
	{'v',      NULL, "Be verbose; May be used multiple times for added verbosity."},
	{'V',      NULL, "Show version information."},
	{'h',      NULL, "Show help."},
	{'4',      NULL, "Use IPv4 only."},
	{'6',      NULL, "Use IPv6 only."},
	//{'c',      NULL, "Check zipfile integrity on boot. Warning! Can be slow!"},
	{0,        NULL, NULL}
};

static const struct optparse_long options_long[] =
{
	//Documented options
	{"help",    'h', OPTPARSE_NONE},
	{"output",  'o', OPTPARSE_REQUIRED},
	{"verbose", 'v', OPTPARSE_NONE},
	{"quiet",   'q', OPTPARSE_NONE},
	{"zipfile", 'z', OPTPARSE_REQUIRED},
	{"port",    'p', OPTPARSE_REQUIRED},
	{"bind",    'b', OPTPARSE_REQUIRED},
	{"check",   'c', OPTPARSE_NONE},
	{"version", 'V', OPTPARSE_NONE},
	{"ipv4",    '4', OPTPARSE_NONE},
	{"ipv6",    '6', OPTPARSE_NONE},
	{NULL,        0, 0}
};

static void version()
{
	//Print httpzip version info
	printf("HTTPzip version x.x.x\r\n");

	//Add library info as well
	printf("Using zlib version %s\r\n", ZLIB_VERSION);
	printf("Using minizip-ng version %s\r\n", MZ_VERSION);
}

/// @brief Show command-line usage.
/// @param name executable name
static void usage(const char* name)
{
	//Usage
	printf("Usage:\r\n");
	printf("  Server: %s [-vq] [-z ZIPFILE] [-p PORT] [-b ADDRESS]\r\n", name);
	printf("  Create: %s [-vq] [-o FILE] [--] ZIPFILE\r\n", name);
	printf("\r\n");
	printf("Options:\r\n");
	// --- Automated documentation of arguments ---
	
	//Iterate over the optparse_long struct and calculate the widest long opt
	const struct optparse_long* option = options_long;
	size_t widest_longopt = 0;
	while (option->shortname)
	{
		size_t longopt_len = strlen(option->longname);
		if (widest_longopt < longopt_len) widest_longopt = longopt_len;
		option++;
	}
	
	//Iterate over the options_description struct and print each argument description
	struct opt_description* desc = options_description;
	while (desc->shortname)
	{
		//Find the entry on options_long
		const struct optparse_long* option = options_long;
		while (option->shortname)
		{
			if (option->shortname == desc->shortname)
			{
				break;
			}
			option++;
		}
		//If not found, skip it
		if (option == NULL)
		{
			desc++;
			continue;
		}

		//Calculate left padding for long options
		size_t left_padding_long = widest_longopt - strlen(option->longname);
		//Calculate left padding for short option
		size_t left_padding_short = strlen(option->longname);
		left_padding_short += left_padding_long;

		//Print short option
		for(size_t i = 0; i < left_padding_short; i++) putchar(' ');
		printf("  -%c", (char) option->shortname);
		if ((option->argtype == OPTPARSE_REQUIRED) || (option->argtype == OPTPARSE_OPTIONAL))
		{
			assert(desc->argument_description);
			printf(" %s", desc->argument_description);
		}
		printf("\r\n");

		//Print long option + description
		for (size_t i = 0; i < left_padding_long; i++) putchar(' ');
		printf("  --%s", option->longname);
		if ((option->argtype == OPTPARSE_REQUIRED) || (option->argtype == OPTPARSE_OPTIONAL))
		{
			printf("=%s", desc->argument_description);
		}
		assert(desc->description);
		printf("  %s\r\n", desc->description);
		desc++;
		printf("\r\n");
	}
}

static void dump_config(const struct hz_config_s* conf)
{
	printf("[DEBUG >]\r\n");
	printf("Program configuration\r\n");
	printf("  General options\r\n");
	printf("      Verbosity: %d\r\n", conf->verbosity);
	printf("     Fast check: %s\r\n", conf->fast_check ? "Yes" : "No");
	printf("\r\n");
	printf("  Server configuration\r\n");
	printf("           Bind: %s\r\n", conf->bind_address ? conf->bind_address : "<DEFAULT>");
	printf("           Port: %d", (int)conf->port);
	if (conf->port == 0) printf(" (Any)");
	printf("\r\n");
	printf("           IPv4: %s\r\n", conf->use_ipv4 ? "Yes" : "No");
	printf("           IPv6: %s\r\n", conf->use_ipv6 ? "Yes" : "No");
	printf("        Zipfile: %s\r\n", conf->zipfile);
	printf("\r\n");
	printf("  Creation configuration\r\n");
	printf("     Input file: %s\r\n", conf->in_name ? conf->in_name : "<NONE>");
	printf("    Output file: %s\r\n", conf->out_name ? conf->out_name : "<DEFAULT>");
	//printf("\r\n");
	printf("[DEBUG <]\r\n");
}

int main(int argc, char** argv)
{
	//Set default options
	static struct hz_config_s hz_conf;
	{
		//General options
		hz_conf.verbosity = HZ_VERBOSITY_DEFAULT;
		hz_conf.fast_check = HZ_DEFAULT_CHECK;

		//Server options
		hz_conf.port = 0; //<-- Any port
		hz_conf.bind_address = NULL; //< Default bind
		hz_conf.zipfile = HZ_DEFAULT_ZIPFILE; //< We are the zipfile
		
		if (HZ_DEFAULT_IPV6)
		{
			hz_conf.use_ipv4 = 0;
			hz_conf.use_ipv6 = 1;
		}
		else
		{
			hz_conf.use_ipv4 = 1;
			hz_conf.use_ipv6 = 0;
		}

		//Creation options
		hz_conf.out_name = NULL; //< Default name
		hz_conf.in_name = NULL; //< No input name
	}

	//Parse command-line options
	{
		struct optparse options;
		optparse_init(&options, argv);

		//Parse named arguments (Arguments like: -s --someting)
		int option;
		while ((option = optparse_long(&options, options_long, NULL)) != -1)
		{
			switch (option)
			{
			case 'h':
				usage(argv[0]);
				return RV_OK;
			case 'v':
				hz_conf.verbosity++;
				break;
			case 'q':
				hz_conf.verbosity = HZ_VERBOSITY_MUTED;
				break;
			case 'o':
				hz_conf.out_name = options.optarg;
				break;
			case 'z':
				hz_conf.zipfile = options.optarg;
				break;
			case 'p':
			{
				int port = atoi(options.optarg);
				if ((port < 1) || (port > 65535))
				{
					printf("[ERROR] Invalid port number\r\n");
					return RV_BAD_OPTIONS;
				}
				hz_conf.port = port;
				break;
			}
			case 'b':
				hz_conf.bind_address = options.optarg;
				break;
			case 'c':
				hz_conf.fast_check = FALSE;
				break;
			case 'V':
				version();
				return RV_OK;
			case '4':
				hz_conf.use_ipv4 = 1;
				hz_conf.use_ipv6 = 0;
				break;
			case '6':
				hz_conf.use_ipv4 = 0;
				hz_conf.use_ipv6 = 1;
				break;
			case '?':
				//Unknown option
				printf("[ERROR] %s\r\n", options.errmsg);
				return RV_BAD_OPTIONS;
			}
		}

		//Parse unnamed arguments (Files and everything past the `--` mark)
		char* arg;
		while ((arg = optparse_arg(&options)))
		{
			hz_conf.out_name = arg;
		}
	}
	if (hz_conf.verbosity >= HZ_VERBOSITY_DEBUG)
		dump_config(&hz_conf);

	//If no zipfile set, use ourselves
	if (hz_conf.zipfile == NULL)
		hz_conf.zipfile = argv[0];

	if (startup_checks(&hz_conf) != RV_OK)
	{
		if (hz_conf.verbosity >= HZ_VERBOSITY_ERROR)
			printf("[ERROR] Self-test failed\r\n");
		return RV_NOK;
	}
	else
	{
		if (hz_conf.verbosity >= HZ_VERBOSITY_DEBUG)
			printf("[DEBUG] Self-test OK\r\n");
	}

	//Launch server
	hz_launch(&hz_conf);

	//Do some cleanup
	{
		//Nothing
		;
	}

	return RV_OK;
}
